package ru.potapov.tm.service;

import ru.potapov.tm.entity.Project;
import ru.potapov.tm.repository.RepositoryProject;

import java.util.Collection;
import java.util.Objects;

public class ServiceProject {
    private RepositoryProject repositoryProject;

    public ServiceProject(RepositoryProject repositoryProject) {
        this.repositoryProject = repositoryProject;
    }

    public RepositoryProject getRepositoryProject() {
        return repositoryProject;
    }

    public int checkSize(){
        return repositoryProject.getCollectionProject().size();
    }

    public Project findProjectByName(String name){
        Project result = null;

        for (Project project : repositoryProject.getCollectionProject()) {
            if (project.getName().equals(name)) {
                result = project;
                break;
            }
        }
        return result;
    }

    public Collection<Project> getCollectionProjects(){
        return repositoryProject.getCollectionProject();
    }

    public Project renameProject(Project project, String name) throws CloneNotSupportedException{
        if (Objects.nonNull(name)){
            Project newProject = (Project) project.clone();
            newProject.setName(name);
            return repositoryProject.merge(newProject);
        }
        return project;
    }
    public void removeAll(){
        repositoryProject.removeAll();
    }

    public void removeAll(Collection<Project> listProjects){
        repositoryProject.removeAll(listProjects);
    }

    public void remove(Project project){
        repositoryProject.remove(project);
    }

    public void put(Project project){
        repositoryProject.merge(project);
    }
}
