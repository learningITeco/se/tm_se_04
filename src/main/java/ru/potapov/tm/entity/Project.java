package ru.potapov.tm.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Project implements Cloneable {
    private String      id;
    private String      name;
    private String      description;
    private LocalDateTime   dateStart;
    private LocalDateTime   dateFinish;

    public Project() {}

    public Project(String name) {
        this.name       = name;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getDateStart() {
        return dateStart;
    }

    public LocalDateTime getDateFinish() {
        return dateFinish;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDateStart(LocalDateTime dateStart) {
        this.dateStart = dateStart;
    }

    public void setDateFinish(LocalDateTime dateFinish) {
        this.dateFinish = dateFinish;
    }

    @Override
    public String toString() {
        return "Project [" + getName() + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (this.getClass() != obj.getClass())
            return false;

        Project objProject = (Project)obj;
        if ( (this.getName().equals(objProject.getName()))
                && (this.getDescription().equals(objProject.getDescription()))
                && (this.getDateStart().equals(objProject.getDateStart()))
                && (this.getDateFinish().equals(objProject.getDateFinish())) )
            return true;
        else
            return false;
    }

    @Override
    public int hashCode() {
        return getName().hashCode() + getDescription().hashCode()+getDateStart().hashCode()+getDateFinish().hashCode();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
