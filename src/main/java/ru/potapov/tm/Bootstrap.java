package ru.potapov.tm;

import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.repository.RepositoryProject;
import ru.potapov.tm.repository.RepositoryTask;
import ru.potapov.tm.service.ServiceProject;
import ru.potapov.tm.service.ServiceTask;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Bootstrap {
    private RepositoryProject   repositoryProject;
    private RepositoryTask      repositoryTask;
    private ServiceProject      serviceProject;
    private ServiceTask         serviceTask;

    private Project             currentProject;
    private DateTimeFormatter   ft;
    private Scanner             in;

    //Constants
    private final String YY = "Y";
    private final String Yy = "y";

    private final String HELP                   = "help";
    private final String EXIT                   = "exit";

    //Constants for Projects:
    private final String CREATE_PROJECT         = "create-project";
    private final String READ_PROJECTS          = "read-projects";
    private final String UPDATE_PROJECT         = "update-project";
    private final String DELETE_ALL_PROJECTS    = "delete-all-projects";
    private final String DELETE_PROJECT         = "delete-project";

    //Constants for Tasks:
    private final String CREATE_TASK        = "create-task";
    private final String READ_TASKS         = "read-tasks";
    private final String READ_ALL_TASKS     = "read-all-tasks";
    private final String UPDATE_TASK        = "update-task";
    private final String DELETE_TASK        = "delete-task";
    private final String DELETE_TASKS       = "delete-tasks";
    private final String DELETE_ALL_TASKS   = "delete-all-tasks";

    public Bootstrap() {
        repositoryProject   = new RepositoryProject();
        repositoryTask      = new RepositoryTask();
        serviceProject      = new ServiceProject(repositoryProject);
        serviceTask         = new ServiceTask(repositoryTask, repositoryProject);

        currentProject      = null;
        ft                  = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    }

    public void init(){
        in = new Scanner(System.in);

    }

    public boolean chooseCommand() {

        System.out.println("\nInsert your command in low case or command <help>:");
        String strIn = in.nextLine();
        String[] arrCommand = strIn.split(" ");

        String cmd = arrCommand[0];

        switch (cmd){
            case HELP:                  commandHelp(); break;
            case EXIT:                  commandExit(); return true;
            case CREATE_PROJECT:        commandCreateProject(); break;
            case READ_PROJECTS:         commandReadProjects(); break;
            case DELETE_ALL_PROJECTS:   commandDeleteAllProjects(); break;
            case DELETE_PROJECT:        commandDeleteProject(); break;
            case UPDATE_PROJECT:        commandUpdateProject(); break;
            case CREATE_TASK:           commandCreateTask(); break;
            case READ_TASKS:            commandReadTasks(); break;
            case READ_ALL_TASKS:        commandReadAllTasks(); break;
            case DELETE_TASK:           commandDeleteTask(); break;
            case DELETE_TASKS:          commandDeleteTasks(); break;
            case DELETE_ALL_TASKS:      commandDeleteAllTasks(); break;
            case UPDATE_TASK:           commandUpdateTask(); break;
            default:                    printNotCorrectCommand();break;
        }

        return false;
    }

    private void printTasksOfProject(Project project, Collection<Task> listTasks){
        System.out.println();
        System.out.println("Project [" + project.getName() + "]:");
        for (Task task : listTasks) {
            if (task.getIdProject().equals(project.getId())){
                System.out.println();
                System.out.println("    Task [" + task.getName() + "]");
                System.out.println("    Description: " + task.getDescription());
                System.out.println("    ID: " + task.getId());
                System.out.println("    Date start: " + task.getDateStart().format(ft));
                System.out.println("    Date finish: " + task.getDateFinish().format(ft));
            }
        }
    }

    public void commandDeleteTask() {
        if (serviceTask.checkSize() == 0){
            System.out.println("We do not have any task.");
            return;
        }

        Task findTask = null;

        boolean circleForName = true;
        while (circleForName){
            System.out.println("Input a task name for remove:");
            String name = in.nextLine();

            if ("exit".equals(name)){
                return;
            }

            findTask = serviceTask.findTaskByName(name);

            if (Objects.isNull(findTask)) {
                System.out.println("Task with name [" + name + "] is not exist, plz try again or type command <exit>");
                continue;
            }

            circleForName = false;
        }

        serviceTask.remove(findTask);
        System.out.println("The task has deleted");
    }

    public void commandUpdateTask() {
        if (serviceTask.checkSize() == 0){
            System.out.println("We do not have any task.");
            return;
        }

        Task findTask = null;

        boolean circleForName = true;
        while (circleForName){
            System.out.println("Input a task name for update:");
            String name = in.nextLine();

            if ("exit".equals(name)){
                return;
            }

            findTask = serviceTask.findTaskByName(name);

            if (Objects.isNull(findTask)) {
                System.out.println("Task with name [" + name + "] is not exist, plz try again or type command <exit>");
                continue;
            }

            circleForName = false;
        }

        System.out.println("Do you want to update the name? <y/n>");
        String answer = in.nextLine();
        switch (answer){
            case YY:
            case Yy:
                System.out.println("Input a new name for this task:");
                try {
                    findTask = serviceTask.renameTask(findTask, in.nextLine());
                    System.out.println("Completed");
                }catch (Exception e){ e.printStackTrace(); }
        }

        System.out.println("Do you want to update the project for this task? <y/n>");
        answer = in.nextLine();
        switch (answer){
            case YY:
            case Yy:
                System.out.println("Input the name of project for this task:");
                Project findProject     = null;
                String name             = in.nextLine();
                try {
                    findProject = serviceProject.findProjectByName(name);
                }catch (Exception e){ e.printStackTrace();}

                if (Objects.isNull(findProject)){
                    System.out.println("Project with name [" + name + "] is not exist, project for this task does not changed");
                }else {
                    try {
                        serviceTask.changeProject(findTask, findProject);
                    }catch (Exception e){ e.printStackTrace(); }
                    System.out.println("Completed");
                }
        }
    }

    public void commandDeleteAllTasks() {
        serviceTask.removeAll();
        System.out.println("All tasks of all projects have deleted");
    }

    public void commandDeleteTasks() {
        if (serviceTask.checkSize() == 0){
            System.out.println("We do not have any task.");
            return;
        }

        boolean circleForProject = true;
        while (circleForProject) {
            System.out.println("Input a project name for removing its tasks:");
            String name = in.nextLine();

            if ("exit".equals(name)){
                return;
            }

            currentProject = serviceProject.findProjectByName(name);

            if (Objects.isNull(currentProject)) {
                System.out.println("Project with name [" + name + "] does not exist, plz try again or type command <exit>");
                continue;
            }

            circleForProject = false;
        }

        Collection<Task> listTaskRemoving = serviceTask.findAll(currentProject.getId());

        serviceTask.removeAll(listTaskRemoving);
        System.out.println("Completed");
    }

    public void commandReadAllTasks() {
        if (serviceTask.checkSize() == 0) {
            System.out.println("We do not have any tasks");
            return;
        }

        for (Project project : serviceProject.getCollectionProjects()) {
            printTasksOfProject(project, serviceTask.findAll(project.getId()));
        }
        System.out.println("Completed");
    }

    public void commandReadTasks() {
        if (serviceTask.checkSize() == 0){
            System.out.println("We have not any project");
            return;
        }

        boolean circleForProject = true;
        while (circleForProject) {
            System.out.println("Input a project name for reading its tasks:");
            String name = in.nextLine();

            if ("exit".equals(name)){
                return;
            }

            currentProject = serviceProject.findProjectByName(name);

            if (Objects.isNull(currentProject)) {
                System.out.println("Project with name [" + name + "] does not exist, plz try again or type command <exit>");
                continue;
            }

            circleForProject = false;
        }

        printTasksOfProject(currentProject, serviceTask.findAll(currentProject.getId()));

        System.out.println("Completed");
    }

    public void commandUpdateProject() {
        Project findProject = null;

        boolean circleForName = true;
        while (circleForName){
            System.out.println("Input a project name for update:");
            String name = in.nextLine();

            if ("exit".equals(name)){
                return;
            }

            findProject = serviceProject.findProjectByName(name);

            if (Objects.isNull(findProject)) {
                System.out.println("Project with name [" + name + "] is not exist, plz try again or type command <exit>");
                continue;
            }

            circleForName = false;
        }

        System.out.println("Do you want to update the name? <y/n>");
        String answer = in.nextLine();
        if ("y".equals(answer) || "Y".equals(answer)){
            System.out.println("Input a new name for this task:");
            try {
                findProject = serviceProject.renameProject(findProject, in.nextLine());
            }catch (Exception e) {e.printStackTrace();}
            System.out.println("Completed");
        }
    }

    public void commandDeleteProject() {
        boolean circleForProject = true;
        while (circleForProject) {
            System.out.println("Input a project name for removing:");
            String name = in.nextLine();

            if ("exit".equals(name)){
                return;
            }

            currentProject = serviceProject.findProjectByName(name);

            if (Objects.isNull(currentProject)) {
                System.out.println("Project with name [" + name + "] does not exist, plz try again or type command <exit>");
                continue;
            }

            circleForProject = false;
        }

        Collection<Task> listTaskRemoving = serviceTask.findAll(currentProject.getId());
        serviceTask.removeAll(listTaskRemoving);
        serviceProject.remove(currentProject);
        System.out.println("Completed");
    }

    public void commandDeleteAllProjects() {
        serviceTask.removeAll();
        serviceProject.removeAll();
        System.out.println("Completed");
    }

    public void commandReadProjects() {
        System.out.println("All projects: \n");
        if (serviceProject.checkSize() == 0){
            System.out.println("We have not any project");
            return;
        }

        for (Project project : serviceProject.getCollectionProjects()) {
            System.out.println();
            System.out.println("Project [" + project.getName() + "]");
            System.out.println("Description: " + project.getDescription());
            System.out.println("ID: " + project.getId());
            System.out.println("Date start: " + project.getDateStart().format(ft));
            System.out.println("Date finish: " + project.getDateFinish().format(ft));
        }

    }

    public void commandCreateProject() {
        boolean circleForName = true;
        while (circleForName){
            System.out.println("Input a name for a new project:");
            String name = in.nextLine();

            currentProject = serviceProject.findProjectByName(name);

            if (Objects.nonNull(currentProject)) {
                System.out.println("Project with name [" + name + "] already exist");
                continue;
            }

            circleForName = false;
            System.out.println("OK");
            currentProject = new Project(name);
        }

        System.out.println("Input a description for this project:");
        currentProject.setDescription(in.nextLine());

        //Date:
        currentProject.setDateStart(inputDate("Input a start date for new project in format<dd-mm-yyyy>:"));
        currentProject.setDateFinish(inputDate("Input a finish date for new project:"));

        currentProject.setId(UUID.randomUUID().toString());

        serviceProject.put(currentProject);
        System.out.println();
        System.out.println("Project [" + currentProject.getName() + "] has created");
        System.out.println("Description: " + currentProject.getDescription());
        System.out.println("ID: " + currentProject.getId());
        System.out.println("Date start: " + currentProject.getDateStart().format(ft));
        System.out.println("Date finish: " + currentProject.getDateFinish().format(ft));
    }

    public void commandCreateTask() {

        Task newTask = new Task();
        boolean idProjectExist = false;

        //IdProject******************
        if (Objects.nonNull(currentProject)) {
            System.out.println("Current project is " + currentProject.getName() + ". Do you want to craete task for this project? <y/n>");
            String answer = in.nextLine();
            if ("y".equals(answer) || "Y".equals(answer)){
                newTask.setIdProject(currentProject.getId());
                idProjectExist = true;
            }
        }

        if (!idProjectExist){
            boolean circleForProject = true;
            while (circleForProject) {
                System.out.println("Input the name of the project for this task:");
                String name = in.nextLine();

                if ("exit".equals(name)){
                    return;
                }

                currentProject = serviceProject.findProjectByName(name);

                if (Objects.isNull(currentProject)) {
                    System.out.println("Project with name [" + name + "] does not exist, plz try again or type command <exit>");
                    continue;
                }

                circleForProject = false;
                newTask.setIdProject(currentProject.getId());
                System.out.println("OK");
            }
        }
        //IdProject******************

        boolean circleForName = true;
        while (circleForName){
            System.out.println("Input a name for a new task:");
            String name = in.nextLine();

            if ("exit".equals(name)){
                return;
            }

            boolean exist = false;

            Task findTask = serviceTask.findTaskByName(name);


            if (Objects.nonNull(findTask)) {
                System.out.println("Task with name [" + name + "] already exist, plz try again or type command <exit>");
                continue;
            }

            circleForName = false;
            System.out.println("OK");
            newTask.setName(name);
        }

        System.out.println("Input a description for this task:");
        newTask.setDescription(in.nextLine());

        //Date:
        newTask.setDateStart(inputDate("Input a start date for new project in format<dd-mm-yyyy>:"));
        newTask.setDateFinish(inputDate("Input a finish date for new project:"));

        newTask.setId(UUID.randomUUID().toString());

        serviceTask.put(newTask);
        System.out.println();
        System.out.println("Task [" + newTask.getName() + "] has created");
        System.out.println("Project: " + currentProject.getName());
        System.out.println("Description: " + newTask.getDescription());
        System.out.println("ID: " + newTask.getId());
        System.out.println("Date start: " + newTask.getDateStart().format(ft));
        System.out.println("Date finish: " + newTask.getDateFinish().format(ft));
    }

    private LocalDateTime inputDate(String massage){
        System.out.println(massage);
        String strDate = in.nextLine();
        LocalDateTime date;
        try {
            int year = Integer.parseInt(strDate.substring(6)),
                    month= Integer.parseInt(strDate.substring(3,5)),
                    day  = Integer.parseInt(strDate.substring(0,2));
            date = LocalDateTime.of(year,month, day,0,0);
        }catch (Exception e){
            System.out.println("Error formate date! Date set to the end of year.");
            date = LocalDateTime.of(LocalDateTime.now().getYear(),12,31,23,59);
        }

        return date;
    }

    public void commandHelp() {
        String help = "There are general commands: \n" +
                "   help:                   lists all command\n" +
                "   exit:                   halts the programm\n" +

                "   create-project:         creates new project\n" +
                "   read-projects:          reads all projects\n" +
                "   update-project:         update name of the Nth project\n" +
                "   delete-project:         deletes the project with name <Name>\n" +
                "   delete-all-projects:    deletes all project\n" +

                "   create-task:            creates new task\n" +
                "   read-tasks:             reads tasks for a current project\n" +
                "   read-all-tasks:         reads all tasks\n" +
                "   update-task:            update name of th Nth task\n" +
                "   delete-task:            deletes the task with name <Name> of the cuurrent project\n" +
                "   delete-tasks:           deletes all tasks of a current project\n" +
                "   delete-all-tasks:       deletes all tasks of all projects\n";
        System.out.println(help);
    }


    public void printNotCorrectCommand() {
        System.out.println("not correct command, plz try again or type command <help> \n");
    }

    private void printNotChoosenCurrentProject() {
        System.out.println("There isn't any choosen projects! Plz, choose a project!");
    }

    private void commandExit() {
        System.out.println("Buy-buy....");
    }
}
