package ru.potapov.tm.repository;

import ru.potapov.tm.entity.Project;

import java.util.*;

public class RepositoryProject extends AbstractRepository<Project>{
    private Map<String, Project> mapProject;

    public RepositoryProject() {
        mapProject = new HashMap<>();
    }

    @Override
    public Collection<Project> findAll() {
        return mapProject.values();
    }

    @Override
    public Project findOne(String name) {
        for (Project project : mapProject.values()) {
            if (project.getName().equals(name)){
                return project;
            }
        }
        return null;
    }

    @Override
    public void persist(Project project) {
        mapProject.putIfAbsent(project.getId(), project);
    }

    @Override
    public Project merge(Project projectNew) {
        return mapProject.put(projectNew.getId(), projectNew);
    }

    @Override
    public void remove(Project project) {
        mapProject.remove(project.getId());
    }

    @Override
    public void removeAll() {
        mapProject.clear();
    }

    @Override
    public void removeAll(Collection<Project> list) {
        for (Project project : list) {
            mapProject.remove(project.getId());
        }
    }

    public Collection<Project> getCollectionProject(){
        return mapProject.values();
    }

}
